import json
import pandas as pd

import sys

sys.path.append("/app/data_hub")

from filter_expr.filter_exprLexer import filter_exprLexer
from filter_expr.filter_exprParser import filter_exprParser
from filter_expr.filter_exprListener import filter_exprListener
from antlr4 import *


try:
    api
except NameError:

    class api:
        @staticmethod
        def send(port, data):
            print("Send data '%s' to '%s'" % (str(data), port))

        @staticmethod
        def set_port_callback(port, callback):
            print(
                "Call '%s' to simulate behavior when messages arrive at port '%s'"
                % (callback.__name__, port)
            )
            # callback()

        @staticmethod
        def add_timer(interval, callback):
            print(
                "Call '%s' to simulate behavior when timer calls the callback."
                % (callback.__name__)
            )
            callback()

        @staticmethod
        def Message(body, attributes={}):
            return Message(body, attributes)

        class config:
            env = "local"  # used to detect the local env
            filter = ""

        class logger:
            @staticmethod
            def info(s):
                print("info: %s" % (s))

            @staticmethod
            def debug(s):
                print("debug: %s" % (s))

            @staticmethod
            def warn(s):
                print("warn: %s" % (s))

            @staticmethod
            def error(s):
                print("error: %s" % (s))

    class Message:
        def __init__(self, body, attributes):
            self.body = body
            self.attributes = attributes

        def __str__(self):
            return json.dumps({"attributes": self.attributes, "body": str(self.body)})


def log_warning(text):
    api.logger.info("MY_WARN: {}".format(text))


class Listener(filter_exprListener):
    def __init__(self):
        self.or_counter = 0
        self.storage_or_counter = []

        self.and_counter = 0
        self.storage_and_counter = []

        self.operators = []
        self.operator = {}
        self.cur_col = ""

        self.result = []

    def enterOr_expr(self, ctx: filter_exprParser.Or_exprContext):
        self.or_counter = 0

    def enterAnd_expr(self, ctx: filter_exprParser.And_exprContext):
        self.and_counter = 0

    def exitAnd_expr(self, ctx: filter_exprParser.And_exprContext):
        self.or_counter += 1
        if self.or_counter >= 2:
            self.result.append("|")

    def enterTerm(self, ctx: filter_exprParser.TermContext):
        sub_expr = ctx.getToken(filter_exprParser.LPAREN, i=0)
        if sub_expr:
            self.storage_or_counter.append(self.or_counter)
            self.storage_and_counter.append(self.and_counter)

    def exitTerm(self, ctx: filter_exprParser.TermContext):
        sub_expr = ctx.getToken(filter_exprParser.RPAREN, i=0)
        if sub_expr:
            self.or_counter = self.storage_or_counter.pop()
            self.and_counter = self.storage_and_counter.pop()

        self.and_counter += 1
        if self.and_counter >= 2:
            self.result.append("&")

    def enterEqual_expr(self, ctx: filter_exprParser.Equal_exprContext):
        pass

    def exitEqual_expr(self, ctx: filter_exprParser.Equal_exprContext):
        self.result.append(self.cur_col)
        self.operators.append(self.operator)
        self.operator = {}

    def enterColumn(self, ctx: filter_exprParser.ColumnContext):
        col = ctx.getToken(ttype=filter_exprParser.WORD, i=0)
        self.operator[col.getText()] = []
        self.cur_col = col.getText()

    def enterOperator(self, ctx: filter_exprParser.OperatorContext):
        operators = ctx.getTokens(ttype=filter_exprParser.WORD)
        self.operator[self.cur_col].append(" ".join([op.getText() for op in operators]))


def interpret_formula(filter_str):
    lexer = filter_exprLexer(InputStream(filter_str))
    token_stream = CommonTokenStream(lexer)
    parser = filter_exprParser(token_stream)
    tree = parser.or_expr()
    print(tree.toStringTree(recog=parser))
    listener = Listener()
    walker = ParseTreeWalker()
    walker.walk(listener, tree)
    print(filter_str)
    return Evaluate(list(reversed(listener.result)), list(reversed(listener.operators)))


class Evaluate:
    def __init__(self, stack, operators):
        self.stack = stack
        self.operators = operators

    def run(self, df):
        from copy import deepcopy
        import operator

        stack_calc = []
        stack = deepcopy(self.stack)
        op_map = {"&": operator.and_, "|": operator.or_}
        while stack:
            var = stack.pop()
            if var in op_map:
                b = stack_calc.pop()
                a = stack_calc.pop()
                stack_calc.append(op_map[var](a, b))
            else:
                restriction = self.operators.pop()
                key = list(restriction)[0]
                mask = False
                for r in restriction[key]:
                    mask = mask | (df[key] == r)
                stack_calc.append(mask)
        return stack_calc[0]


def is_formula_correct(filter_str):
    lexer = filter_exprLexer(InputStream(filter_str))
    token_stream = CommonTokenStream(lexer)
    parser = filter_exprParser(token_stream)
    tree = parser.query()
    return True if not parser.getNumberOfSyntaxErrors() else False


"""
your operator coding
"""


def filter_df(in1):
    try:
        # if receive message
        if isinstance(in1.body, Exception):
            api.send("output", in1)
            return
        elif isinstance(in1.body, pd.DataFrame):
            in1 = in1.body
        else:
            raise Exception("UNKNOWN MESSAGE TYPE \"{}\"".format(type(in1.body)))


        mask_evaluator = interpret_formula(api.config.filter)

        #check columns in df
        for op_dict in mask_evaluator.operators:
            col_name = list(op_dict)[0]
            if not col_name in in1:
                raise Exception("NO COLUMN {} ON INPUT".format(col_name))

        in1 = in1[mask_evaluator.run(in1)]

        #send result
        api.send("result", api.Message(in1))
    except Exception as e:
        log_warning(str(e))
        api.send("output", api.Message(e))


def on_generate():
    # check is formula correct
    if not is_formula_correct(api.config.filter):
        api.send("output", api.Message(Exception("Error in filter formula: {}".format(api.config.filter))))
        api.propagate_exception(Exception("Error in filter formula: {}".format(api.config.filter)))



"""
Starting the operator
"""
# api.add_generator(on_generate)
api.set_port_callback("in1", filter_df)

# detecting environment (local vs. vflow)
try:
    api.config.env
    print("Operator started in local environment")

    # mock an incoming message from a previous operator
    api.config.filter = '((a=a1;"a2 test";a3 test or d=b2) and c=c3 or d = e2) and a=a1'
    if not is_formula_correct(api.config.filter):
        print("Error in filter formula: {}".format(api.config.filter))
    msg = api.Message(
        pd.DataFrame(
            {"a": ["a1", "a2"], "b": ["b1", "b2"], "c": ["c1", "c2"], "d": ["e2", "e3"]}
        ),
        {},
    )
    filter_df(msg)
except AttributeError:
    print("Operator started in productive mode")
