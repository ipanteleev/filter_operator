# Generated from C:/Users/ipanteleev/PycharmProjects/FilterOperator\filter_expr.g4 by ANTLR 4.7.2
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .filter_exprParser import filter_exprParser
else:
    from filter_exprParser import filter_exprParser

# This class defines a complete listener for a parse tree produced by filter_exprParser.
class filter_exprListener(ParseTreeListener):

    # Enter a parse tree produced by filter_exprParser#query.
    def enterQuery(self, ctx:filter_exprParser.QueryContext):
        pass

    # Exit a parse tree produced by filter_exprParser#query.
    def exitQuery(self, ctx:filter_exprParser.QueryContext):
        pass


    # Enter a parse tree produced by filter_exprParser#or_expr.
    def enterOr_expr(self, ctx:filter_exprParser.Or_exprContext):
        pass

    # Exit a parse tree produced by filter_exprParser#or_expr.
    def exitOr_expr(self, ctx:filter_exprParser.Or_exprContext):
        pass


    # Enter a parse tree produced by filter_exprParser#and_expr.
    def enterAnd_expr(self, ctx:filter_exprParser.And_exprContext):
        pass

    # Exit a parse tree produced by filter_exprParser#and_expr.
    def exitAnd_expr(self, ctx:filter_exprParser.And_exprContext):
        pass


    # Enter a parse tree produced by filter_exprParser#term.
    def enterTerm(self, ctx:filter_exprParser.TermContext):
        pass

    # Exit a parse tree produced by filter_exprParser#term.
    def exitTerm(self, ctx:filter_exprParser.TermContext):
        pass


    # Enter a parse tree produced by filter_exprParser#equal_expr.
    def enterEqual_expr(self, ctx:filter_exprParser.Equal_exprContext):
        pass

    # Exit a parse tree produced by filter_exprParser#equal_expr.
    def exitEqual_expr(self, ctx:filter_exprParser.Equal_exprContext):
        pass


    # Enter a parse tree produced by filter_exprParser#column.
    def enterColumn(self, ctx:filter_exprParser.ColumnContext):
        pass

    # Exit a parse tree produced by filter_exprParser#column.
    def exitColumn(self, ctx:filter_exprParser.ColumnContext):
        pass


    # Enter a parse tree produced by filter_exprParser#operator.
    def enterOperator(self, ctx:filter_exprParser.OperatorContext):
        pass

    # Exit a parse tree produced by filter_exprParser#operator.
    def exitOperator(self, ctx:filter_exprParser.OperatorContext):
        pass


