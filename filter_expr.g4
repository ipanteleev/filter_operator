grammar filter_expr;

query: or_expr EOF;

or_expr: and_expr (OR and_expr)*;

and_expr: term (AND term)*;

term: equal_expr | LPAREN or_expr RPAREN;

equal_expr: column EQUAL operator (COMMA operator)* COMMA?;

column: WORD;

operator: QUOTE WORD+ QUOTE | WORD+;

LPAREN: '(';

RPAREN: ')';

AND: 'AND'|'and'|'&';

OR: 'OR'|'or'|'|'|'||';

COMMA: ';'|',';

QUOTE: '"'|'\'';

EQUAL: '='|'==';

WORD: LETTER+;

LETTER: ('A'..'Z') | ('a'..'z') | ('А'..'Я') | ('а'..'я') | ('0'..'9') | '_' | '.' | '-';

WS
   : [ \r\n\t] + -> skip
;